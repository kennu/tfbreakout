#!/usr/bin/env python
# TensorFlow Breakout (C) Kenneth Falck 2016
from __future__ import print_function
import app
import traceback
import os
import sys
import signal

# Main application
def main():
    app.init()
    app.main_loop()

def sigterm_handler(signo, stack):
    try:
        app.uninit()
    except:
        pass
    sys.exit(0)

# Autostart main application; catch errors and grafecully end curses
if __name__ == '__main__':
    signal.signal(signal.SIGTERM, sigterm_handler)
    try:
        main()
    except:
        ex = sys.exc_info()
        try:
            app.uninit()
        except:
            pass
        print('Handled exception.')
        traceback.print_exception(*ex)
    try:
        app.uninit()
    except:
        pass
    print('Exited.')
