# TensorFlow Breakout (C) Kenneth Falck 2016
import time
from collections import namedtuple
import tensorflow as tf
import numpy
import random
import logging

# How many pixel frames to keep in history
PIXEL_HISTORY_SIZE = 100
NUM_ACTIONS = 3
INITIAL_EPSILON = 1.0 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon
EXPLORE_TIME = 100000 # how many ticks to decay epsilon over
OBSERVE_TIME = 50000 # how many ticks to observe and collect data before training
REPLAY_BUFFER_SIZE = 50000
SAMPLE_SIZE = 75
GAMMA = 0.7 # decay rate of past observations

Transition = namedtuple('Transition', ['previous_state', 'action', 'reward', 'next_state', 'game_over'])

class LogHandler(logging.StreamHandler):
    def __init__(self, info_view):
        self.info_view = info_view
    def emit(self, record):
        msg = self.format(record)
        self.info_view.log('LOG: %s' % (msg))
        self.flush()

class AIBot(object):
    # Initialize player bot. This is a TensorFlow based AI bot.
    def __init__(self, game, info_view):
        self.game = game
        self.info_view = info_view
        self.last_stats_time = time.time()
        self.tick_count = 0
        self.last_action = 0
        self.last_reward = 0
        self.previous_state = None#[] # store last 4 pixel frames here
        logger = logging.getLogger('tensorflow')
        logger.handlers = []
        rootLogger = logging.getLogger()
        rootLogger.setLevel(logging.DEBUG)
        handler = LogHandler(self.info_view)
        handler.setLevel(logging.DEBUG)
        rootLogger.handlers = []
        rootLogger.addHandler(handler)

        config = tf.ConfigProto(
            log_device_placement=True
        )
        #if True:
        #    NUM_CORES = 8
        #    config.inter_op_parallelism_threads = NUM_CORES
        #    config.intra_op_parallelism_threads = NUM_CORES
        #    self.info_view.log('AIBot: Creating AI bot with %d cores' % (NUM_CORES))
        self.session = tf.Session(config=config)
        self.create_model()

    # End bot session at exit
    def end(self):
        self.session.close()

    # Restart game (after game over)
    def restart(self):
        self.info_view.log('AIBot: Starting new game.')
        self.last_action = 0
        self.last_reward = 0
        self.previous_state = None#[]

    # Paddle hit event
    def on_paddle_hit(self):
        self.info_view.log('AIBot: Register paddle hit')
        self.last_reward = 1

    # Block hit event
    def on_block_hit(self):
        self.info_view.log('AIBot: Register block hit (score %d)' % (self.game.score))
        #self.last_reward = 1

    # Level cleared event
    def on_level_clear(self):
        self.info_view.log('AIBot: Level cleared (score %d)' % (self.game.score))

    # Game over event
    def on_game_over(self):
        self.info_view.log('AIBot: Game over.')
        #self.last_reward = -1
        # Simulate final terminal tick
        self.tick(game_over=True)

    # Print current statistics
    def print_stats(self):
        if time.time() - self.last_stats_time  >= 10:
            self.last_stats_time = time.time()

    # Process one game tick
    def tick(self, game_over=False):
        self.print_stats()
        # Get current game pixels, add at end of old frames (keep only 4)
        current_state = self.game.get_game_pixels(self.previous_state)
        # Run the AI model; it will return the best predicted action
        if self.previous_state:
            action = self.run_model(self.tick_count, self.previous_state, self.last_action, self.last_reward, current_state, game_over)
            if action == 1 and not game_over:
                # Move left
                self.game.move_paddle_left()
            elif action == 2 and not game_over:
                # Move right
                self.game.move_paddle_right()
            self.last_action = action
        # Clear last reward at end of tick; will be set again if next tick produces reward
        self.last_reward = 0
        # Store this state as the previous state before returning from tick
        self.previous_state = current_state
        self.tick_count += 1

    # Create a reinforcing Q learning model
    def create_model(self):
        self.info_view.log('Creating Q learning model with size %dx%dx%d' % (self.game.rows, self.game.cols, 4))
        # Input layer
        #s = tf.placeholder('float', [None, 80, 80, 4])
        s = tf.placeholder('float', [None, self.game.rows, self.game.cols, 4]) # 4 frames

        conv_1_stride_y = 4
        conv_1_stride_x = 4
        if self.game.rows < 80:
            conv_1_stride_y = 2
        if self.game.cols < 80:
            conv_1_stride_x = 2


        # Hidden layer 1 (convolution)
        W_conv1 = tf.Variable(tf.truncated_normal([8, 8, 4, 32], stddev=0.01)) # weight
        b_conv1 = tf.Variable(tf.constant(0.01, shape=[32])) # bias
        h_conv1 = tf.nn.relu(tf.nn.conv2d(s, W_conv1, strides=[1, conv_1_stride_y, conv_1_stride_x, 1], padding="SAME") + b_conv1)
        h_pool1 = tf.nn.max_pool(h_conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")

        # Hidden layer 2 (convolution)
        W_conv2 = tf.Variable(tf.truncated_normal([4, 4, 32, 64], stddev=0.01)) # weight
        b_conv2 = tf.Variable(tf.constant(0.01, shape=[64])) # bias
        h_conv2 = tf.nn.relu(tf.nn.conv2d(h_pool1, W_conv2, strides=[1, 2, 2, 1], padding="SAME") + b_conv2)
        h_pool2 = tf.nn.max_pool(h_conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")

        # Hidden layer 3 (convolution)
        W_conv3 = tf.Variable(tf.truncated_normal([3, 3, 64, 64], stddev=0.01)) # weight
        b_conv3 = tf.Variable(tf.constant(0.01, shape=[64])) # bias
        tc = tf.nn.conv2d(h_pool2, W_conv3, strides=[1, 1, 1, 1], padding="SAME")
        h_conv3 = tf.nn.relu(tc + b_conv3)
        h_pool3 = tf.nn.max_pool(h_conv3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")
        h_pool3_flat = tf.reshape(h_pool3, [-1, 256])

        # fc1
        W_fc1 = tf.Variable(tf.truncated_normal([256, 256], stddev=0.01)) # weight
        b_fc1 = tf.Variable(tf.constant(0.01, shape=[256])) # bias
        h_fc1 = tf.nn.relu(tf.matmul(h_pool3_flat, W_fc1) + b_fc1)

        # Readout layer
        W_fc2 = tf.Variable(tf.truncated_normal([256, NUM_ACTIONS], stddev=0.01)) # weight
        b_fc2 = tf.Variable(tf.constant(0.01, shape=[NUM_ACTIONS])) # bias
        q_readout = tf.matmul(h_fc1, W_fc2) + b_fc2

        # Define cost function
        a = tf.placeholder('float', [None, NUM_ACTIONS])
        y = tf.placeholder('float', [None])
        q_readout_action = tf.reduce_sum(tf.mul(q_readout, a), reduction_indices=1)
        cost = tf.reduce_mean(tf.square(y - q_readout_action))
        train_step = tf.train.AdamOptimizer(1e-6).minimize(cost)

        # Restore saved neural network state
        saver = tf.train.Saver()
        self.session.run(tf.initialize_all_variables())
        checkpoint = tf.train.get_checkpoint_state('saved_networks')
        if checkpoint and checkpoint.model_checkpoint_path:
            saver.restore(self.session, checkpoint.model_checkpoint_path)
            self.info_view.log('AIBot: Restored neural network state: %s' % (checkpoint.model_checkpoint_path))

        # Store the model in the bot object
        self.model = (s, a, y, q_readout, train_step, saver)
        self.epsilon = INITIAL_EPSILON
        self.replay_buffer = []

    def log_state(self, current_state):
        with open('state.txt', 'a') as out:
            lines = ['', '', '', '']
            for y in range(0, self.game.rows):
                for x in range(0, self.game.cols):
                    for f in range(0, 4):
                        color = current_state[y][x][f]
                        lines[f] += ('%d' % color) if color > 0 else ' '
                for i in range(0, len(lines)):
                    lines[i] += '\n'

            for line in lines:
                out.write(line)
            out.write('\n')

    def log_q(self, current_q):
        q = numpy.max(current_q)
        qi = int(q*500)
        with open('q.txt', 'a') as out:
            if qi < 0:
                qi = -qi
                out.write(' ' * (50-qi) + '*' * qi + ('|') + ' ' * 50 + '\n')
            else:
                out.write(' ' * 50 + '|' + '*' * qi + '\n')

    def action_to_bits(self, action):
        return (1 if action==0 else 0, 1 if action==1 else 0, 1 if action==2 else 0)

    # Train reinforcing Q learning model with transition from previous state to current state
    # This function will return the next recommended action
    def run_model(self, t, previous_state, action, reward, current_state, game_over):
        #self.info_view.log('AIBot: Training model with %d history to reward %d' % (len(history), reward))

        if t % 40 == 0:
            self.log_state(current_state)

        if reward != 0:
            self.info_view.log('AIBot: Reward %d for action=%d t=%d replays=%d game_over=%s' % (reward, action, t, len(self.replay_buffer), game_over))

        # Restore model variables created earlier
        s, a, y, q_readout, train_step, saver = self.model

        # Add the transition to the replay buffer
        self.replay_buffer.append(Transition(previous_state, self.action_to_bits(action), reward, current_state, game_over))
        while len(self.replay_buffer) > REPLAY_BUFFER_SIZE:
            self.replay_buffer.pop(0)

        if t > OBSERVE_TIME and len(self.replay_buffer) > SAMPLE_SIZE:
            # Take a random sample from the replay buffer
            sample_set = random.sample(self.replay_buffer, SAMPLE_SIZE)
            #sample_set = self.replay_buffer[-1:]
            sample_previous_states = [trans.previous_state for trans in sample_set]
            sample_actions = [trans.action for trans in sample_set]
            sample_rewards = [trans.reward for trans in sample_set]
            sample_actions = [trans.action for trans in sample_set]
            sample_next_states = [trans.next_state for trans in sample_set]
            sample_game_overs = [trans.game_over for trans in sample_set]

            # Calculate Q values for each next_state in the sample set
            sample_q_values = q_readout.eval(session=self.session, feed_dict={
                s: sample_next_states
            })

            # Add max Q values to the rewards in the sample
            adjusted_rewards = [reward + GAMMA * max(sample_q_values[i]) if not sample_game_overs[i] else reward for i, reward in enumerate(sample_rewards)]
            #self.info_view.log('AIBot: Adjusted rewards: %s' % (adjusted_rewards))

            # Train model with the adjusted data
            train_step.run(session=self.session, feed_dict={
                s: sample_previous_states,
                a: sample_actions,
                y: adjusted_rewards,
            })

            # Save neural network state every now and then
            if t % 100 == 0:
                self.info_view.log('AIBot: Saving state t=%d epsilon=%.3f' % (t, self.epsilon))
                saver.save(self.session, 'saved_networks/saved-dqn', global_step=t)


        # Choose best next action based on current state
        if random.random() <= self.epsilon:
            # Use random action instead of recommended action
            action = random.randrange(NUM_ACTIONS)
            #self.info_view.log('AIBot: Chose random action %d (epsilon=%.3f)' % (action, self.epsilon))
        else:
            # Use recommended action
            current_q = q_readout.eval(session=self.session, feed_dict={
                s: [current_state]
            })[0]
            action = numpy.argmax(current_q)
            self.info_view.log('AIBot: Chose recommended action %d (epsilon=%.3f) (%s)' % (action, self.epsilon, current_q))
            self.log_q(current_q)

        # Scale down epsilon every step
        if self.epsilon > FINAL_EPSILON:
            self.epsilon -= (INITIAL_EPSILON - FINAL_EPSILON) / EXPLORE_TIME # scale epsilon down over N ticks

        # Return the recommended action; the next run_model() will reflect its outcome
        return action
