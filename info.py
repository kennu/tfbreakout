# TensorFlow Breakout (C) Kenneth Falck 2016
import curses
import datetime
import textwrap

class InfoView(object):
    # Initialize info window
    def __init__(self, win):
        y, x = win.getbegyx()
        rows, cols = win.getmaxyx()
        self.rows = rows-2
        self.cols = cols-4
        self.outer_win = win
        self.win = curses.newwin(self.rows, self.cols, y+1, x+2)
        self.win.scrollok(1)
        self.lines = []

    def redraw(self):
        self.outer_win.redrawwin()
        self.outer_win.refresh()
        self.win.redrawwin()
        self.win.refresh()

    # Write a line in the info log
    def log(self, text):
        now = datetime.datetime.now()
        parts = textwrap.wrap(text, self.cols-12, break_long_words=True)
        for row, part in enumerate(parts):
            if row == 0:
                line = '[%02d:%02d:%02d] %s' % (now.hour, now.minute, now.second, part)
            else:
                line = '           %s' % (part)
            self.lines.append(line)
            while len(self.lines) > self.rows:
                self.lines.pop(0)
                self.win.scroll(1)
            self.win.addstr(len(self.lines)-1, 0, line)
        self.win.noutrefresh()
