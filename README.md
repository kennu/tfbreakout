# TensorFlow Breakout
(C) Kenneth Falck <kennu@iki.fi> 2016

## Running locally in a virtualenv

    mkvirtualenv tfbreakout
    pip install -r requirements.txt
    ./breakout

## Setting up for development

In addition to the above instructions, run this to auto-restart on .py changes:

    npm install -g nodemon
    make
