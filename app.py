# TensorFlow Breakout (C) Kenneth Falck 2016
from __future__ import print_function
import atexit
import curses
import game
import colors
import time
import simplebot
import aibot
import info

# Duration of one game tick
#TICK_TIME = 0.05
#SLEEP_TIME = 0.001
TICK_TIME = 0
SLEEP_TIME = 0

# Main curses window
stdscr = None

# Game
active_game = None
highscore = 0
totalscore = 0
totalgames = 0

# Initialize curses and UI
def init():
    global stdscr
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    curses.start_color()
    curses.curs_set(0)
    stdscr.keypad(1)
    stdscr.timeout(0)
    atexit.register(uninit)
    init_colors()

# Define color pairs
def init_colors():
    curses.init_pair(colors.TOP_TITLE, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.init_pair(colors.TOP_HELP, curses.COLOR_YELLOW, curses.COLOR_BLUE)
    curses.init_pair(colors.BACKGROUND, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(colors.PADDLE, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(colors.BALL, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(colors.GAME_OVER, curses.COLOR_WHITE, curses.COLOR_RED)
    curses.init_pair(colors.BLOCKS[0], curses.COLOR_BLACK, curses.COLOR_RED)
    curses.init_pair(colors.BLOCKS[1], curses.COLOR_BLACK, curses.COLOR_YELLOW)
    curses.init_pair(colors.BLOCKS[2], curses.COLOR_BLACK, curses.COLOR_GREEN)
    curses.init_pair(colors.BLOCKS[3], curses.COLOR_BLACK, curses.COLOR_BLUE)

def print_score():
    global highscore
    if active_game.score > highscore:
        highscore = active_game.score
    stdscr.addstr(0, 16, '  Score: %-3s Avg: %-5.1f High: %-3s' % (active_game.score, float(totalscore)/float(totalgames) if totalgames > 0 else 0, highscore), curses.color_pair(colors.TOP_TITLE))

# Run main application loop forever until Q is pressed
def main_loop():
    global active_game
    rows, cols = stdscr.getmaxyx()

    title = 'TensorFlow Breakout (C) Kenneth Falck 2016'
    stdscr.addstr(0, 0, ''.ljust(cols), curses.color_pair(colors.TOP_TITLE))
    stdscr.addstr(0, cols/2-len(title)/2, title, curses.color_pair(colors.TOP_TITLE))
    stdscr.addstr(0, 1, 'Press Q to Quit', curses.color_pair(colors.TOP_HELP))
    stdscr.refresh()

    prev_time = time.time()
    restart_time = time.time()
    quit = False

    # Create the game window, info window and the game
    game_cols = 40#(cols/16)*8
    info_cols = cols-game_cols

    info_win = curses.newwin(rows-1, info_cols, 1, game_cols)
    info_win.box()
    info_win.noutrefresh()
    info_view = info.InfoView(info_win)
    info_view.log('Creating game with grid size %dx%d' % (game_cols, rows-1))

    game_win = curses.newwin(rows-1, game_cols, 1, 0)
    active_game = game.Game(game_win)

    # Add a bot to play the game
    bot = None
    #bot = simplebot.SimpleBot(active_game)
    bot = aibot.AIBot(active_game, info_view)
    if bot:
        bot.restart()

    # Level won handler
    def on_level_clear(game):
        print_score()
        if bot:
            bot.on_level_clear()

    # Paddle hit handler
    def on_paddle_hit(game):
        print_score()
        if bot:
            bot.on_paddle_hit()

    # Block hit handler
    def on_block_hit(game):
        print_score()
        if bot:
            bot.on_block_hit()

    # Game over handler
    def on_game_over(game):
        global totalgames, totalscore
        print_score()
        if bot:
            if bot.epsilon < 0.5:
                totalscore += active_game.score
                totalgames += 1
            bot.on_game_over()
        stdscr.addstr(0, cols-10, 'GAME OVER', curses.color_pair(colors.GAME_OVER))
        restart_time = time.time() + 5

    active_game.on_level_clear = on_level_clear
    active_game.on_paddle_hit = on_paddle_hit
    active_game.on_block_hit = on_block_hit
    active_game.on_game_over = on_game_over

    while not quit:
        ch = stdscr.getch()
        if ch == curses.KEY_LEFT:
            active_game.move_paddle_left()
        if ch == curses.KEY_RIGHT:
            active_game.move_paddle_right()
        if ch == ord('q'):
            quit = True
        if ch == ord('r'):
            active_game.restart()
            if bot:
                bot.restart()
        if ch == 12 or ch == ord('l'):
            stdscr.redrawwin()
            stdscr.refresh()
            game_win.redrawwin()
            game_win.refresh()
            info_view.redraw()
        elapsed = time.time() - prev_time
        if elapsed >= TICK_TIME:
            prev_time = time.time()
            if bot:
                bot.tick()
            active_game.tick()
            if not active_game.game_is_over and bot:
                stdscr.addstr(0, cols-20, '%7sms %9s' % (int(active_game.avg_tick_time()*1000), bot.tick_count), curses.color_pair(colors.TOP_TITLE))
            elif time.time() >= restart_time:
                active_game.restart()
                print_score()
                if bot:
                    bot.restart()
            curses.doupdate()
        if SLEEP_TIME > 0:
            time.sleep(SLEEP_TIME)

    if bot:
        bot.end()

# Uninitialize curses and UI (called automatically at exit)
def uninit():
    global stdscr
    if stdscr is not None:
        stdscr.keypad(0)
        stdscr = None
        curses.curs_set(0)
        curses.nocbreak()
        curses.echo()
        curses.endwin()
