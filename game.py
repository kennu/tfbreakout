# TensorFlow Breakout (C) Kenneth Falck 2016
from __future__ import print_function
import curses
import colors
import time
import copy
import random

class Game(object):
    # Initialize game
    def __init__(self, win):
        self.win = win
        self.rows, self.cols = win.getmaxyx()
        self.on_level_clear = None
        self.on_block_hit = None
        self.on_game_over = None
        self.pixelbuf = []
        for row in range(0, self.rows):
            self.pixelbuf.append([0 for col in range(0, self.cols)])
        self.restart()

    # Restart game
    def restart(self):
        # Reset game
        self.score = 0
        self.blocks = [
            [True, True, True, True, True, True, True, True, ],
            [True, True, True, True, True, True, True, True, ],
            [True, True, True, True, True, True, True, True, ],
            [True, True, True, True, True, True, True, True, ],
        ]
        self.block_width = self.cols / len(self.blocks[0])
        self.block_height = self.rows / 7
        if self.block_height <= 0:
            self.block_height = 1
        self.win.clear()
        for y in range(0, self.rows):
            for x in range(0, self.cols):
                self.pixelbuf[y][x] = 0
        self.game_is_over = False
        self.ticks = 0
        self.last_tick_time = time.time()
        self.cumul_tick_time = 0
        # Reset paddle
        self.paddle_width = 10
        self.paddle_half = self.paddle_width/2
        self.paddle_y = self.rows-2
        self.paddle_x = (self.cols/2 - self.paddle_width/2) / self.paddle_width * self.paddle_width
        # Reset ball
        self.ball_size = 2
        self.ball_x = random.randrange(2, self.cols - 4)
        self.ball_y = self.rows-3
        self.ball_vertical_direction = -1
        self.ball_horizontal_direction = 1
        # Draw everything
        self.draw_paddle()
        self.draw_blocks()
        self.draw_ball()
        self.win.noutrefresh()

    # Process one tick of the game (every 100ms)
    def tick(self):
        if self.game_is_over:
            return
        self.cumul_tick_time += time.time() - self.last_tick_time
        self.last_tick_time = time.time()
        self.ticks += 1
        self.move_ball()
        self.win.noutrefresh()

    def avg_tick_time(self):
        return self.cumul_tick_time / self.ticks if self.ticks > 0 else 0

    # Draw the paddle in its current position
    def draw_paddle(self):
        self.addstr(self.paddle_y, 0, ''.ljust(self.cols), colors.BACKGROUND)
        self.addstr(self.paddle_y, self.paddle_x, ''.ljust(self.paddle_width), colors.PADDLE)

    def set_paddle_position(self, pos):
        self.paddle_x = pos*self.paddle_width
        self.draw_paddle()

    # Move the paddle (can be called interactively or by automation)
    def move_paddle_left(self, amount=None):
        if amount is None:
            amount = self.paddle_half
        if self.paddle_x-amount >= 0:
            self.paddle_x -= amount
            self.draw_paddle()
        elif self.paddle_x > 0:
            self.paddle_x = 0
            self.draw_paddle()

    # Move the paddle (can be called interactively or by automation)
    def move_paddle_right(self, amount=None):
        if amount is None:
            amount = self.paddle_half
        if self.paddle_x+amount < self.cols-self.paddle_width:
            self.paddle_x += amount
            self.draw_paddle()
        elif self.paddle_x < self.cols-self.paddle_width:
            self.paddle_x = self.cols-self.paddle_width
            self.draw_paddle()

    # Draw the blocks at top
    def draw_blocks(self):
        for y, block_row in enumerate(self.blocks):
            for x, block in enumerate(block_row):
                for i in range(0, self.block_height):
                    self.addstr(y*self.block_height+i, x*self.block_width, ''.ljust(self.block_width), colors.BLOCKS[y] if block else colors.BACKGROUND)

    # Return the block indexes (row, col) at specified coords
    def get_block_index_at(self, y, x):
        row, col = y/self.block_height, x/self.block_width
        if row < 0 or row >= len(self.blocks):
            return -1, -1
        if col < 0 or col >= len(self.blocks[0]):
            return -1, -1
        return row, col

    # Draw the ball, which is always in movement
    def draw_ball(self):
        self.addstr(self.ball_y, self.ball_x, '  ', colors.BALL)

    # Clear the ball from old location
    def clear_ball(self):
        self.addstr(self.ball_y, self.ball_x, '  ', colors.BACKGROUND)

    # Move the ball to current direction
    def move_ball(self):
        new_y = self.ball_y + self.ball_vertical_direction
        new_x = self.ball_x + self.ball_horizontal_direction

        # Check if ball is hitting a wall
        need_bounce = False

        # Check if ball is hitting top
        if self.ball_vertical_direction == -1 and new_y < 0:
            # Bounce from top
            self.ball_vertical_direction = 1
            need_bounce = True

        # Check if ball is hitting left wall
        if self.ball_horizontal_direction == -1 and new_x < 0:
            self.ball_horizontal_direction = 1
            need_bounce = True

        # Check if ball is hitting right wall
        if self.ball_horizontal_direction == 1 and new_x >= self.cols-2:
            self.ball_horizontal_direction = -1
            need_bounce = True

        # Check if ball is hitting paddle at bottom
        if self.ball_vertical_direction == 1 and new_y == self.paddle_y and self.paddle_x-1 <= new_x and new_x < self.paddle_x+self.paddle_width:
            # Bounce from paddle
            self.ball_vertical_direction = -1
            self.paddle_hit()
            need_bounce = True

        # Check if ball is hitting a block
        block_row, block_col = self.get_block_index_at(new_y, new_x)
        if block_row >= 0 and block_col >= 0:
            if self.blocks[block_row][block_col]:
                # Hit a block!
                #self.blocks[block_row][block_col] = False
                self.draw_blocks()
                self.block_hit()
                if not max(max(row) for row in self.blocks):
                    # All blocks gone
                    return self.level_clear()
                self.ball_vertical_direction *= -1
                need_bounce = True

        # If the direction was changed, calculate again
        if need_bounce:
            return self.move_ball()

        # Check if ball is falling below game area
        if new_y < 0 or new_y >= self.rows:
            self.clear_ball()
            return self.game_over()

        # Ball has space to move, move it
        self.clear_ball()
        self.ball_y = new_y
        self.ball_x = new_x
        self.draw_ball()

    # Register a paddle hit
    def paddle_hit(self):
        self.score += 1
        if self.on_paddle_hit is not None:
            self.on_paddle_hit(self)

    # Register a block hit
    def block_hit(self):
        #self.score += 1
        if self.on_block_hit is not None:
            self.on_block_hit(self)

    # Enter game won state (all blocks cleared)
    def level_clear(self):
        if self.on_level_clear is not None:
            self.on_level_clear(self)
        self.restart()

    # Enter game over state
    def game_over(self):
        self.game_is_over = True
        # Call event if defined
        if self.on_game_over is not None:
            self.on_game_over(self)

    # Write to screen (maintain a copy in pixelbuf)
    def addstr(self, y, x, s, color):
        self.win.addstr(y, x, s, curses.color_pair(color))
        for i, ch in enumerate(s):
            self.pixelbuf[y][x+i] = 1.0 if color in (colors.PADDLE, colors.BALL) else 0.0

    # Return game state as pixels (array of rows, which are arreys of cols)
    def get_game_pixels(self, prevbuf):
        #pixels = []
        #for y in range(0, self.rows):
        #    row = []
        #    for x in range(0, self.cols):
        #        row.append((self.win.inch(y, x) and 0xff00) >> 8)
        #    pixels.append(row)
        #return copy.deepcopy(self.pixelbuf)
        buf = []
        for y in range(0, self.rows):
            row = []
            for x in range(0, self.rows):
                if prevbuf:
                    # Shift previous state
                    frame = [prevbuf[y][x][1], prevbuf[y][x][2], prevbuf[y][x][3], self.pixelbuf[y][x]]
                else:
                    # First state
                    frame = [0, 0, 0, self.pixelbuf[y][x]]
                row.append(frame)
            buf.append(row)
        return buf
        #return [list(row) for row in self.pixelbuf]
