# TensorFlow Breakout (C) Kenneth Falck 2016

class SimpleBot(object):
    # Initialize player bot. This is a non-AI bot.
    def __init__(self, game, info_view):
        self.game = game
        self.info_view = info_view

    # End bot session at exit
    def end(self):
        pass

    # Restart game (after game over)
    def restart(self):
        self.info_view.log('Initializing SimpleBot.')

    # Paddle hit event
    def on_paddle_hit(self):
        self.info_view.log('AIBot: Register paddle hit')

    # Block hit event
    def on_block_hit(self):
        self.info_view.log('SimpleBot: Register block hit (score %d)' % (self.game.score))

    # Signal level cleared
    def on_level_clear(self):
        self.info_view.log('SimpleBot: Level cleared!')

    # Signal game over
    def on_game_over(self):
        self.info_view.log('SimpleBot: Game over.')

    # Process one game tick
    def tick(self):
        # Try to center the paddle on the ball
        if self.game.ball_x < self.game.paddle_x+self.game.paddle_half:
            return self.game.move_paddle_left()
        if self.game.ball_x > self.game.paddle_x+self.game.paddle_half:
            return self.game.move_paddle_right()
